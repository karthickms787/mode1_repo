package com.main;

import java.util.Scanner;

import com.exceptn.UserException;
import com.model.userinfo;
import com.service.UserService;
import com.service.UserServiceImpl;

public class loginAppMain {

	public static void main(String[] args) {
		int uId;
		String uPwd;
		Scanner sc = new Scanner(System.in);
		userinfo user = new userinfo();
		UserService obj = new UserServiceImpl();
		try {
			System.out.println("Enter the UserID:");
			uId = sc.nextInt();
			if (uId <= 0) {
				System.err.println("Enter the Positive Number");
			}
			System.out.println("Enter the password:");
			uPwd = sc.next();
			user.setUserId(uId);
			user.setPassword(uPwd);

			String uName = obj.ValidateUser(user);
			if (uName.length() > 0) {
				System.out.println("Welcome" + uName);
			} else {
				System.out.println("Sorry No such User");
			}

		} catch (UserException e) {
			System.err.println(e.getMessage());

		} finally {
			obj = null;
			user = null;
			sc = null;
		}

	}

}
