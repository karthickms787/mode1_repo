package com.exceptn;

public class UserException extends Exception {
	private String message;

	public UserException(String message) {
		super();
		this.message = message;
	}


	public String getMessage() {
		return message;
	}

}
