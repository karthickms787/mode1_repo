package day3;

import java.util.Scanner;

public class Test3main {

	public static void main(String[] args) {
		Test3 test3 = new Test3();
		int cunt = test3.count;
		int tmp = test3.temp;
		// User inputs the array size
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the no of elements you want in array: ");
		cunt = sc.nextInt();
		int num[] = new int[cunt];
		System.out.println("Enter the array elements:");
		for (int i = 0; i < cunt; i++) {
			num[i] = sc.nextInt();

		}
		sc.close();
		for (int i = 0; i < cunt; i++) {
			for (int j = i + 1; j < cunt; j++) {
				if (num[i] > num[j]) {
					tmp = num[i];
					num[i] = num[j];
					num[j] = tmp;

				}

			}
		}
		System.out.println("Array elements in ascending order:");
		for (int i = 0; i < cunt - 1; i++) {
			System.out.println(num[i] + ",");
		}
		System.out.println(num[cunt - 1]);

	}

}
