package day3;

import java.util.Scanner;
import java.io.*;

public class Test8 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String s = sc.next();

		StringBuffer sb = new StringBuffer();
		if (s.charAt(0) == 'j') {
			if (s.charAt(1) == 'b') {
				sb.append(s);
			} else {
				sb.append(s.charAt(0)).append(s.substring(2));
			}
		} else if (s.charAt(1) == 'b') {
			sb.append(s.charAt(0)).append(s.substring(2));
		} else {
			sb.append(s.substring(2));
		}
		System.out.println(sb);
	}

}
