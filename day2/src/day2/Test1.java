package day2;

import java.util.Scanner;

public class Test1 {

	private static Scanner sc;

	public static void main(String[] args) {
		sc = new Scanner(System.in);
		System.out.print("Input the first number: ");
		double x = sc.nextDouble();
		System.out.print("Input the Second number: ");
		double y = sc.nextDouble();
		System.out.print("Input the third number: ");
		double z = sc.nextDouble();
		System.out.print("The smallest value is " + smallest(x, y, z) + "\n");
	}

	public static double smallest(double x, double y, double z) {
		return Math.min(Math.min(x, y), z);
	}

}
