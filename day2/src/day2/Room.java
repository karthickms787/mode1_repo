package day2;

/*Test No:4
 * Write a program to create a room class, the attributes of this class is roomno, roomtype, roomarea and ACmachine. In this class 
 * the member functions are setdata and displaydata.
 */
public class Room {
	int roomNo;
	String roomType;
	float roomArea;
	boolean acMachine;

	void setData(int rno, String rt, float area, boolean ac) {
		roomNo = rno;
		roomType = rt;
		roomArea = area;
		acMachine = ac;

	}

	void displayData() {
		System.out.println("The room #. Is " + roomNo);
		System.out.println("The room Type is " + roomType);
		System.out.println("The room area is " + roomArea);
		String s = (acMachine) ? "yes" : "no";
		System.out.println("The A/c Machine needed " + s);
	}

}
