package day2;

public class AbInherit {

	public static void main(String[] args) {
		A superOb = new A();
		B subOb = new B();

		superOb.i = 10;
		superOb.j = 20;
		System.out.println("Contents of superOb:");
		superOb.showij();
		System.out.println();

		subOb.i = 1;
		subOb.j = 2;
		subOb.k = 3;
		System.out.println("Contents of supOb:");
		subOb.showij();
		subOb.showk();
		System.out.println();
		System.out.println("Sum of i, j and k in subob:");
		subOb.sum();

	}

}
