package day1;

import java.util.Scanner;

public class Test5 {

	private static Scanner sc;

	public static void main(String[] args) {
		boolean status = true;
		int num = 0;
		sc = new Scanner(System.in);
		System.out.println("Enter the number:");
		num = sc.nextInt();
		if ((num == 0) || (num == 1)) {
			System.out.println("number is not a prime");
		} else {
			for (int i = 2; i <= num / 2; i++) {
				if (num % i == 0) {
					System.out.println("number is not a prime");
					status = false;
					break;
				}
			}
			if (status == true) {
				System.out.println("the number is prime");
			}

		}

	}

}
