package day1;
/*
 * Write a Java program to swap two variables.
 */

public class Test4 {
	static int a = 10;
	static int b = 30;
	static int temp;

	public static void main(String[] args) {
		temp = a;
		a = b;
		b = temp;

		System.out.println("The Swap value of a :" + a);
		System.out.println("The Swap value of a :" + b);

	}

}
