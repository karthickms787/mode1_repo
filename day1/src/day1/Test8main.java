package day1;

import java.util.*;

public class Test8main {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		int n=sc.nextInt();
		int r=Test8class.checksum(n);
		if(r==1) {
			System.out.println("The sum of odd digits are odd");
		}
		else {
			System.out.println("The sum of odd digits are even");
		}
		sc.close();

	}

}
