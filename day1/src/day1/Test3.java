package day1;
/*
 * Write a Java program that takes three numbers as input to calculate and 
 * print the average of the numbers.
 */

import java.util.Scanner;

public class Test3 {

	private static Scanner accept;

	public static void main(String[] args) {
		accept = new Scanner(System.in);
		System.out.println("Enter first number");
		int a = accept.nextInt();
		System.out.println("Enter Second number");
		int b = accept.nextInt();
		System.out.println("Enter Third number");
		int c = accept.nextInt();

		System.out.println("The average of three numbers is" + (a + b + c) / 3);

	}

}
