package day5;
/*
 * Question 3
 */
import java.util.*;
import java.util.Scanner;
public class Mycalculator {
	int power(int n, int p) throws Exception {
		if (n < 0 || p < 0)
			throw new Exception("n and p should be non-negative");
		return (int) Math.pow((double) n, (double) p);
	}
}